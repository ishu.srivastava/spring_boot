package com.gl.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringJpaApplication.class, args);
		System.out.println("@@@@@@@@@@@@ Application is running on port number 9090 @@@@@@@@@@@@@@@");
	}

}
