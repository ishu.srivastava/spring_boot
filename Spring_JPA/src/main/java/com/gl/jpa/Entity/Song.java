package com.gl.jpa.Entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
public class Song {
    @Embedded
    private SongId id;
    @Id
    @SequenceGenerator(
            name ="song_Id",
            sequenceName = "song_Id",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "song_Id"
    )
    private Long songId;
    private int duration;
    private String genre;
    private LocalDateTime releaseDate;
    private int rating;
    private String downloadUrl;

    public Song(){
        //No args cons
    }

    public SongId getId() {
        return id;
    }

    public void setId(SongId id) {
        this.id = id;
    }

    public Long getSongId() {
        return songId;
    }

    public void setSongId(Long songId) {
        this.songId = songId;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public Song(SongId id, Long songId, int duration, String genre, LocalDateTime releaseDate, int rating, String downloadUrl) {
        this.id = id;
        this.songId = songId;
        this.duration = duration;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.rating = rating;
        this.downloadUrl = downloadUrl;
    }

    @Override
    public String toString() {
        return "Song{" +
                "id=" + id +
                ", songId=" + songId +
                ", duration=" + duration +
                ", genre='" + genre + '\'' +
                ", releaseDate=" + releaseDate +
                ", rating=" + rating +
                ", downloadUrl='" + downloadUrl + '\'' +
                '}';
    }
}
