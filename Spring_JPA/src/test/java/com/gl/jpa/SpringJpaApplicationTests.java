package com.gl.jpa;

import com.gl.jpa.Entity.Song;
import com.gl.jpa.Entity.SongId;
import com.gl.jpa.Repository.SongRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.auditing.CurrentDateTimeProvider;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
class SpringJpaApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private SongRepo repo;

	@Test
	public void saveSong(){
		SongId songId=SongId.builder()
				.name("DDLJ")
				.album("Movie")
				.artist("Arjit")
				.build();

		LocalDateTime time= LocalDateTime.now();

		Song song= Song.builder()
				.duration(15)
				.genre("hindi")
				.releaseDate(null)
				.rating(3)
				.downloadUrl("http://download.this.song1")
				.id(songId)
				.releaseDate(time)
				.build();

		repo.save(song);


	}

	@Test
	public void findAllSong(){
		List<Song> songList=repo.findAll();

		System.out.println("SongList:- "+songList);

	}

	@Test
	public void findByGenre(){
		List<Song> song=repo.findByGenre("hindi");
		System.out.println("SongGenreList:- "+song);
	}


}
