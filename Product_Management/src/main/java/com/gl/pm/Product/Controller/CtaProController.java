package com.gl.pm.Product.Controller;

import com.gl.pm.Product.Entity.Category;
import com.gl.pm.Product.Entity.Product;
import com.gl.pm.Product.Services.CtaProService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Products")
public class CtaProController {

    @Autowired
    private CtaProService service;

    @PostMapping("/addPro")
    public Product addProduct(@RequestBody Product product){
        return service.addProduct(product);
    }

    @PostMapping("/addCat")
    public Category addCategory(@RequestBody Category category){
        return  service.addCategory(category);
    }

    @GetMapping("/findPro/{id}")
    public Product findProductById(@PathVariable("id") Long productCode ){
        return service.findProductById(productCode);
    }

    @GetMapping("/category/{id}")
    public Category findCategoryById(@PathVariable("id") Long CategoryCode ){
        return service.findCategoryById(CategoryCode);
    }

    @GetMapping("/listProduct")
    public List<Product> findAllProduct(){
        return service.findAllProducts();
    }

    @GetMapping("/listCategory")
    public List<Category> findAllCategory(){
        return service.findAllCategory();
    }

    @GetMapping("/findProBy/{name}")
    public Product findProductByName(@PathVariable("name") String productName){
        return service.findProductByName(productName);
    }

    @GetMapping("/findCatBy/{name}")
    public Category findCategoryByType(@PathVariable("name") String CategoryType ){
        return service.findCategoryByType(CategoryType);
    }

    @PutMapping("/update/{id}")
    public Product updateProductById(@PathVariable("id") Long productCode, @RequestBody Product product ){
        return service.updateProductById(productCode,product);
    }
    @DeleteMapping("/delete/{id}")
    public String deleteProductById(@PathVariable("id") Long productCode){
        service.deleteProductById(productCode);
        return "Data Deleted Successfully";
    }

}
