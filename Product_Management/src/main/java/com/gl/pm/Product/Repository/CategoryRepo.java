package com.gl.pm.Product.Repository;

import com.gl.pm.Product.Entity.Category;
import com.gl.pm.Product.Entity.CategoryPkId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableJpaRepositories
public interface CategoryRepo extends JpaRepository<Category, Long> {
}
