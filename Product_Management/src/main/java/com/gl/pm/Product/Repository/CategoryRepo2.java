package com.gl.pm.Product.Repository;

import com.gl.pm.Product.Entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

@Repository
@EnableJpaRepositories
public interface CategoryRepo2 extends JpaRepository<Category,String> {
}
