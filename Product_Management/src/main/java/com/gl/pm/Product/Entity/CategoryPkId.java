package com.gl.pm.Product.Entity;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode
public class CategoryPkId implements Serializable {
    private Long CategoryCode;
    private String CategoryType;

    public CategoryPkId(){

    }

    public Long getCategoryCode() {
        return CategoryCode;
    }

    public void setCategoryCode(Long categoryCode) {
        CategoryCode = categoryCode;
    }

    public String getCategoryType() {
        return CategoryType;
    }

    public void setCategoryType(String categoryType) {
        CategoryType = categoryType;
    }

    public CategoryPkId(Long categoryCode, String categoryType) {
        CategoryCode = categoryCode;
        CategoryType = categoryType;
    }

    @Override
    public String toString() {
        return "CategoryPkId{" +
                "CategoryCode=" + CategoryCode +
                ", CategoryType='" + CategoryType + '\'' +
                '}';
    }
}
