package com.gl.pm.Product.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity
@IdClass(CategoryPkId.class)
public class Category {

    @Id
    private Long CategoryCode;
    @Id
    private String CategoryType;
    private String CategoryBrand;

    public Category(){

    }

    public Long getCategoryCode() {
        return CategoryCode;
    }

    public void setCategoryCode(Long categoryCode) {
        CategoryCode = categoryCode;
    }

    public String getCategoryType() {
        return CategoryType;
    }

    public void setCategoryType(String categoryType) {
        CategoryType = categoryType;
    }

    public String getCategoryBrand() {
        return CategoryBrand;
    }

    public void setCategoryBrand(String categoryBrand) {
        CategoryBrand = categoryBrand;
    }

    public Category(Long categoryCode, String categoryType, String categoryBrand) {
        CategoryCode = categoryCode;
        CategoryType = categoryType;
        CategoryBrand = categoryBrand;
    }

    @Override
    public String toString() {
        return "Category{" +
                "CategoryCode=" + CategoryCode +
                ", CategoryType='" + CategoryType + '\'' +
                ", CategoryBrand='" + CategoryBrand + '\'' +
                '}';
    }
}
