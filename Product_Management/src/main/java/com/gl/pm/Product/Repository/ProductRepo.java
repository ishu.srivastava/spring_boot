package com.gl.pm.Product.Repository;

import com.gl.pm.Product.Entity.Product;
import com.gl.pm.Product.Entity.ProductPkId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableJpaRepositories
public interface ProductRepo extends JpaRepository<Product, Long> {

}
